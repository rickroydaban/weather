package rickroydaban.android.weathermap;

import android.app.Application;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        DependencyAssembly.init(this);
    }
}
