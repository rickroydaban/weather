package rickroydaban.android.weathermap;

import android.content.Context;

import rickroydaban.android.weathermap.modules.main.MainActivity;
import rickroydaban.android.weathermap.modules.main.MainInteractorImpl;
import rickroydaban.android.weathermap.modules.main.MainPresenter;
import rickroydaban.android.weathermap.modules.main.MainPresenterImpl;
import rickroydaban.android.weathermap.modules.weather_detail.WeatherDetailActivity;
import rickroydaban.android.weathermap.utils.network.Webservice;
import rickroydaban.android.weathermap.utils.network.sample.SampleWebservice;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class DependencyAssembly {
    private static DependencyAssembly instance;

    public static void init(Context context){
        if(instance == null)
            instance = new DependencyAssembly(context);
    }

    public static DependencyAssembly get() {
        return instance;
    }

    private Context context;
    private Webservice webservice;
    public DependencyAssembly(Context context){
        this.context = context;
        webservice = new SampleWebservice(context);
    }

    public MainPresenter mainPresenter(MainActivity activity){ return new MainPresenterImpl(activity, new MainInteractorImpl(webservice)); }

}
