package rickroydaban.android.weathermap.modules.main;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import rickroydaban.android.weathermap.R;
import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;

/**
 * Created by Rick Royd Aban on 12/01/2018.
 */
public class Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<WeatherInfo> weatherInfos;

    public Adapter(List<WeatherInfo> weatherInfos){
        this.weatherInfos = weatherInfos;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(final ViewGroup parent, int viewType) {
        return new Holder(LayoutInflater.from(parent.getContext()).inflate(R.layout.row_weather_info, parent, false));
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, int position) {
        Holder holder = (Holder)viewHolder;
        WeatherInfo weatherInfo = weatherInfos.get(position);
        holder.tvTemp.setText(String.valueOf(weatherInfo.getTemp()));
        holder.tvLocation.setText(weatherInfo.getLocation());
        holder.tvWeather.setText(weatherInfo.getActualWeather());
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return weatherInfos.size();
    }


    public class Holder extends RecyclerView.ViewHolder{
        @BindView(R.id.tviews_temp) TextView tvTemp;
        @BindView(R.id.tviews_location) TextView tvLocation;
        @BindView(R.id.tviews_weather) TextView tvWeather;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}