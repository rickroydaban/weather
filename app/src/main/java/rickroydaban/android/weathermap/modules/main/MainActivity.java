package rickroydaban.android.weathermap.modules.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import rickroydaban.android.weathermap.DependencyAssembly;
import rickroydaban.android.weathermap.R;
import rickroydaban.android.weathermap.modules.main.views.ButtonFragment;
import rickroydaban.android.weathermap.modules.main.views.ListFragment;
import rickroydaban.android.weathermap.modules.weather_detail.WeatherDetailActivity;

public class MainActivity extends AppCompatActivity implements MainView{
    MainPresenter presenter;
    @BindView(R.id.fragment_list) View cList;
    @BindView(R.id.fragment_button) View cButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        presenter = DependencyAssembly.get().mainPresenter(this);
        getFragmentManager().beginTransaction().replace(R.id.fragment_list, new ListFragment()).replace(R.id.fragment_button, new ButtonFragment()).commit();
    }

    @Override
    public void onWeatherItemUpdated(String location) {
        Toast.makeText(this, "Weather for "+location+" updated!", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onWeatherItemReceiveFailed(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateDetails(Bundle extras) {
        Intent intent = new Intent(this, WeatherDetailActivity.class);
        intent.putExtras(extras);
        startActivity(intent);
    }

    public MainPresenter getPresenter(){ return presenter; }
}
