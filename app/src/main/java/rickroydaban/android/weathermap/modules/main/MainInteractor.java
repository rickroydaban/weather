package rickroydaban.android.weathermap.modules.main;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface MainInteractor {
    void setOutput(MainInteractorOut out);
    void getWeatherInformation();
}
