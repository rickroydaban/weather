package rickroydaban.android.weathermap.modules.main;

import java.util.ArrayList;
import java.util.List;

import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;
import rickroydaban.android.weathermap.utils.CallbackWeatherInfo;
import rickroydaban.android.weathermap.utils.network.Webservice;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class MainInteractorImpl implements MainInteractor {
    MainInteractorOut out;
    Webservice webservice;

    public MainInteractorImpl(Webservice webservice){
        this.webservice = webservice;
    }

    @Override
    public void setOutput(MainInteractorOut out) {
        this.out = out;
    }

    @Override
    public void getWeatherInformation() {
        List<String> locations = new ArrayList<>();
        locations.add("London");
        locations.add("Prague");
        locations.add("San Francisco");
        webservice.getWeatherInformation(locations, new CallbackWeatherInfo() {
            @Override
            public void onStart() {
                out.onWeatherItemReceiveStarted();
            }

            @Override
            public void onReceive(WeatherInfo weatherInfo) {
                out.onWeatherItemReceiveSuccess(weatherInfo);
            }

            @Override
            public void onFailure(String message) {
                out.onWeatherItemReceiveFailed(message);
            }
        });
    }

}
