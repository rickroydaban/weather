package rickroydaban.android.weathermap.modules.main;

import java.util.List;

import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface MainInteractorOut {
    void onWeatherItemReceiveStarted();
    void onWeatherItemReceiveSuccess(WeatherInfo weatherInfo);
    void onWeatherItemReceiveFailed(String message);
}
