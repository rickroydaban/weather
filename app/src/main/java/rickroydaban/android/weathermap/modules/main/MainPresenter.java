package rickroydaban.android.weathermap.modules.main;

import android.support.v7.widget.RecyclerView;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface MainPresenter {
    void init(RecyclerView recyclerView);
    void refresh();
}
