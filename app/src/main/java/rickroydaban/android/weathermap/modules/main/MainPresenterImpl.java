package rickroydaban.android.weathermap.modules.main;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;
import rickroydaban.android.weathermap.modules.weather_detail.WeatherDetailActivity;
import rickroydaban.android.weathermap.utils.RecyclerItemClickListener;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class MainPresenterImpl implements MainPresenter, MainInteractorOut {
    private MainView view;
    private MainInteractor interactor;
    List<WeatherInfo> weatherInfos;
    Adapter adapter;

    public MainPresenterImpl(MainView view, MainInteractor interactor){
        this.view = view;
        this.interactor = interactor;
        interactor.setOutput(this);
    }

    @Override
    public void init(RecyclerView recyclerView) {
        weatherInfos = new ArrayList<>();
        recyclerView.setLayoutManager(new LinearLayoutManager(recyclerView.getContext()));
        adapter = new Adapter(weatherInfos);
        recyclerView.setAdapter(adapter);
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(recyclerView.getContext(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override public void onItemClick(View v, int position) {
                WeatherInfo weatherInfo = weatherInfos.get(position);
                Bundle extras = new Bundle();
                extras.putString(WeatherDetailActivity.INTENTKEY_LOCATION, weatherInfo.getLocation());
                extras.putString(WeatherDetailActivity.INTENTKEY_WEATHER, weatherInfo.getActualWeather());
                extras.putDouble(WeatherDetailActivity.INTENTKEY_TEMP, weatherInfo.getTemp());
                view.navigateDetails(extras);
            }
        }));
        refresh();
    }

    @Override
    public void refresh() {
        interactor.getWeatherInformation();
    }

    @Override
    public void onWeatherItemReceiveStarted() {
        weatherInfos.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onWeatherItemReceiveSuccess(WeatherInfo weatherInfo) {
        weatherInfos.add(weatherInfo);
        adapter.notifyDataSetChanged();
        view.onWeatherItemUpdated(weatherInfo.getLocation());
    }

    @Override
    public void onWeatherItemReceiveFailed(String message) {
        view.onWeatherItemReceiveFailed(message);
    }
}
