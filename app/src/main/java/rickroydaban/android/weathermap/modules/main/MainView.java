package rickroydaban.android.weathermap.modules.main;

import android.os.Bundle;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface MainView {
    void onWeatherItemUpdated(String location);
    void onWeatherItemReceiveFailed(String message);
    void navigateDetails(Bundle extras);
}
