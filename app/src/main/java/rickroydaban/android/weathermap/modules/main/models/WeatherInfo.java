package rickroydaban.android.weathermap.modules.main.models;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class WeatherInfo {
    private String location;
    private String actualWeather;
    private double temp;

    public WeatherInfo(String location, String actualWeather, double temp){
        this.location = location;
        this.actualWeather = actualWeather;
        this.temp = temp;
    }

    public String getLocation(){ return location; }
    public String getActualWeather(){ return actualWeather; }
    public double getTemp(){ return temp; }
}
