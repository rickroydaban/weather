package rickroydaban.android.weathermap.modules.main.views;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.OnClick;
import rickroydaban.android.weathermap.R;
import rickroydaban.android.weathermap.modules.main.MainActivity;

/**
 * Created by rickroydaban on 04/05/2018.
 */

public class ButtonFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getActivity()).inflate(R.layout.fragment_main_button, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @OnClick(R.id.buttons_refresh) public void onRefresh(){
        ((MainActivity)getActivity()).getPresenter().refresh();
    }
}
