package rickroydaban.android.weathermap.modules.main.views;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import rickroydaban.android.weathermap.R;
import rickroydaban.android.weathermap.modules.main.MainActivity;
import rickroydaban.android.weathermap.modules.main.MainPresenter;

/**
 * Created by rickroydaban on 04/05/2018.
 */

public class ListFragment extends Fragment {
    @BindView(R.id.recyclerview) RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.fragment_main_list, container, false);
        ButterKnife.bind(this, view);
        ((MainActivity)getActivity()).getPresenter().init(recyclerView);
        return view;
    }
}
