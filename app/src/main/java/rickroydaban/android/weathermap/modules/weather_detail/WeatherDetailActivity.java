package rickroydaban.android.weathermap.modules.weather_detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import rickroydaban.android.weathermap.DependencyAssembly;
import rickroydaban.android.weathermap.R;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class WeatherDetailActivity extends AppCompatActivity {
    public static final String INTENTKEY_TEMP = "temp";
    public static final String INTENTKEY_WEATHER = "weather";
    public static final String INTENTKEY_LOCATION = "location";

    @BindView(R.id.tviews_weather) TextView tvWeather;
    @BindView(R.id.tviews_location) TextView tvLocation;
    @BindView(R.id.tviews_temp) TextView tvTemp;
    @BindView(R.id.iviews_icon) ImageView ivIcon;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather_info);
        ButterKnife.bind(this);

        tvWeather.setText(getIntent().getStringExtra(INTENTKEY_WEATHER));
        tvLocation.setText(getIntent().getStringExtra(INTENTKEY_LOCATION));
        tvTemp.setText(String.valueOf(getIntent().getDoubleExtra(INTENTKEY_TEMP, 0)));
        Picasso.with(this).load("https://image.ibb.co/j30yZS/icon_weather.png").resize(((int)getResources().getDimension(R.dimen.img)), ((int)getResources().getDimension(R.dimen.img))).centerCrop().into(ivIcon);
    }
}
