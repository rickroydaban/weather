package rickroydaban.android.weathermap.utils;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class AppConfig {
    public static final String REST_ENDPOINT = "https://api.openweathermap.org/data/2.5/";
}
