package rickroydaban.android.weathermap.utils;

import java.util.List;

import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface CallbackWeatherInfo {
    void onStart();
    void onReceive(WeatherInfo weatherInfo);
    void onFailure(String message);
}
