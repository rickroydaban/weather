package rickroydaban.android.weathermap.utils.network;

import android.provider.SyncStateContract;

import java.io.IOException;
import java.util.Map;
import java.util.Set;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import rickroydaban.android.weathermap.utils.AppConfig;

/**
 * Created by rickroydaban on 04/05/2018.
 */

public class NetworkService {

    private static HttpLoggingInterceptor setHttpLogger() {
        HttpLoggingInterceptor interceptor;
        interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    private static OkHttpClient setHttpClient(final Map<String, String> headers) {
        OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = null;
                if(headers != null){
                    Set<String> keys = headers.keySet();
                    for(String key : keys) {
                        request = chain.request().newBuilder().addHeader(key, headers.get(key)).build();
                    }
                }
                return chain.proceed(request);
            }
        });
        return new OkHttpClient.Builder().addInterceptor(setHttpLogger()).build();
    }

    public static Retrofit restful(Map<String, String> headers) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(AppConfig.REST_ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create())
                .client(setHttpClient(headers))
                .build();
        return retrofit;
    }

}