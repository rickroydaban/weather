package rickroydaban.android.weathermap.utils.network;

import java.util.List;

import rickroydaban.android.weathermap.utils.CallbackWeatherInfo;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public interface Webservice {
    void getWeatherInformation(List<String> locations, CallbackWeatherInfo callback);
}
