package rickroydaban.android.weathermap.utils.network.sample;

import android.content.Context;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rickroydaban.android.weathermap.modules.main.models.WeatherInfo;
import rickroydaban.android.weathermap.utils.CallbackWeatherInfo;
import rickroydaban.android.weathermap.utils.network.NetworkService;
import rickroydaban.android.weathermap.utils.network.Webservice;

/**
 * Created by rickroydaban on 03/05/2018.
 */

public class SampleWebservice implements Webservice {
    private Context context;

    public SampleWebservice(Context context){
        this.context = context;
    }


    @Override
    public void getWeatherInformation(List<String> locations, final CallbackWeatherInfo callback) {
        WeatherInfoAPI httpClient = NetworkService.restful(null).create(WeatherInfoAPI.class);
        callback.onStart();
        for(String location :locations){
            System.out.println("TESTX location "+location);
            Call<JsonObject> call = httpClient.getWeatherByLocation(location, "98f95b003e1535ffaa7b9d7922a19672");
            call.enqueue(new Callback<JsonObject>() {
                @Override
                public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                    System.out.println("TESTX call "+call.request().url());
                    if(response.isSuccessful()){
                        System.out.println("TESTX get weather response "+response.body());
                        JsonObject result = response.body();
                        String weather = result.get("weather").getAsJsonArray().get(0).getAsJsonObject().get("main").getAsString();
                        String location = result.get("name").getAsString();
                        double temp = result.get("main").getAsJsonObject().get("temp").getAsDouble();
                        WeatherInfo weatherInfo = new WeatherInfo(location, weather, temp);
                        callback.onReceive(weatherInfo);
                    }else{
                        System.out.println("TESTX failed "+response.errorBody());
                        callback.onFailure(response.message());
                    }
                }

                @Override
                public void onFailure(Call<JsonObject> call, Throwable t) {
                    callback.onFailure(t.getMessage());
                }
            });
        }
    }
}
