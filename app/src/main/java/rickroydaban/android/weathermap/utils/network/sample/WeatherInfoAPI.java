package rickroydaban.android.weathermap.utils.network.sample;

import com.google.gson.JsonObject;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by rickroydaban on 04/05/2018.
 */

public interface WeatherInfoAPI {

    @GET("weather")
    Call<JsonObject> getWeatherByLocation(@Query("q") String location, @Query("appid") String appID);
}
